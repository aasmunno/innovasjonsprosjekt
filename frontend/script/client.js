var socket = io.connect('localhost:3030', {secure: true});

socket.on("clientConnected", function(id) {
    console.log("Connected to server with id: " + id);
});

socket.on("updateData", function(sessionData) {
    chartConfig.data.datasets[0].data = sessionData; 
    if(router.currentRoute.fullPath == "/results") {
        chart.update();
    }
});

socket.on("sessionResults", function(results) {
    sessionResults = results;

    chartConfig.data.datasets[0].data = sessionResults.results[0]; 
    if(router.currentRoute.fullPath == "/results") {
        chart.update();
    }

    for(let i=0; i<5; i++) {
        if(app.$children[i].results!=undefined) {
            app.$children[i].results = sessionResults;
            break;
        }
    }
});

socket.on("clientUpdate", function(newClientNum) {
    clientNum = newClientNum;
    for(let i=0; i<5; i++) {
        if(app.$children[i].clientNum!=undefined) {
            app.$children[i].clientNum = clientNum;
            break;
        }
    }
});