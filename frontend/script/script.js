var chart = null;

var dataObj = "";
var clientNum = 0;
var sessionResults = "";

var chartConfig = {
    type: "bar",
    data: {
        labels: ["Grønn", "Lilla", "Rød", "Blå"],
        datasets: [{
            label: "Farger",
            data: [0,0,0,0],
            backgroundColor: [
                "#2BEF55",
                "#BA7EF5",
                "#FD0E0E",
                "#3AA5FF"
            ]
        }]
    },
    options: {
        scales: {
            y: {
                suggestedMax: 20
            }
        },
        plugins: {
            
        }
    }
}

var testData = {
    title: "Undersøkelse Elsys",
    questions:  [
        "Hvordan trives du på Elsys?",
        "Synes du fagene er utfordrende?",
        "Hvor mange fag tar du?",
        "Må HS gå?"
    ],
    alts: [
        [
            "Jeg trives veldig godt!",
            "Godt",
            "Ikke så godt",
            "Dårlig"
        ],
        [
            "De er veldig utfordrende!",
            "Litt utfordrende",
            "Litt lett",
            "Easy peasy lemon squizy"
        ],
        [
            "1",
            "2",
            "3",
            "4"
        ],
        [
            "HS",
            "MÅ",
            "GÅ",
            "!!"
        ]
    ]
}

class newData {
    constructor(title, questions, alts) {
        this.title = title,
        this.questions = questions,
        this.alts = alts
    }
}

function getIndex(clientID, connectedClients) {
    for(let i=0; i<connectedClients.length; i++) {
        if(connectedClients[i].id==clientID) {
        return i;
        }
    }
}

function admin() {
    socket.emit("admin");

    document.getElementById("header_container").style.visibility = "visible";
    document.getElementById("header_container").style.height = "16vh";
    document.getElementById("menu").style.visibility = "visible";
    document.getElementById("menu").style.height = "6vh";
}

function reset() {
    socket.emit("reset");
}

function uuidv4() {
    return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
      (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}