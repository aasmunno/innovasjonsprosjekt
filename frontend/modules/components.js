//Vue components
Vue.component("page-head", {
  template: `
  <div id="header_container">
    <div id="banner"> <h1> Innovasjonsprosjekt </h1> </div>
  </div>
  `
})

Vue.component("main-menu", {
  template: `
    <div id="menu">
      <div v-on:click="navigate(0)" class="menu_button active"> Operator </div>
      <div v-on:click="navigate(1)" class="menu_button"> Results </div>
      <div v-on:click="navigate(2)" class="menu_button"> Client </div>
      <div id="clientNumContainer"> Connected clients: <div id="clientNum"> {{ clientNum }} </div> </div>
    </div>
  `,
  data() {
    return {
      clientNum: clientNum
    }
  },
  methods: {
    navigate(destination) {

      this.updateButtons(destination);

      switch (destination) {
        case 0:
          if(router.currentRoute.fullPath!="/operator") {
            this.$router.push("/operator");
          };
          break;

        case 1:
          if(router.currentRoute.fullPath!="/results") {
            this.$router.push("/results");
          }
          break;

        case 2:
          if(router.currentRoute.fullPath!="/client") {
            this.$router.push("/client");
          };
          break;
      }
    },
    updateButtons(destination) {
      var buttons = document.getElementsByClassName("menu_button");

      for(let i=0; i<buttons.length; i++){
        buttons[i].classList.remove("active");
      }

      if(destination<42) {
        buttons[destination].classList.add("active");
      }

    },
    getCurrentPage() {

      switch (router.currentRoute.fullPath) {
        case "/operator":
          return 0;

        case "/results":
          return 1; 

        case "/client":
          return 2;

        case "/404error":
          return 69

      }
    }
  },
  mounted() {
    this.updateButtons(this.getCurrentPage());
  }
})
  
Vue.component("page-foot", {
  template: `
    <div id="footer_container">
      <div id="copyright"> Innovasjonsprosjekt - Gruppe 24 </div>
    </div>
  `
})

Vue.component("question", {
  template: `
    <div id="question_spec" ref="question">
      <div id="question">
        <input class="question_input" type="text" :placeholder="'Question ' + (index+1) + '...'" required></input>
        <button id="remove_question" v-on:click="removeQuestion()"> Remove question </button>
      </div>
      <input class="alt_input" v-for="n in alternatives" :key="n.id" :placeholder="n.placeholder"></input>
    </div>
  `,
  props: {
    index: Number
  },
  data() {
    return {
      alternatives: [
        { id: uuidv4(), placeholder: "Alternative 1..." },
        { id: uuidv4(), placeholder: "Alternative 2..." },
        { id: uuidv4(), placeholder: "Alternative 3..." },
        { id: uuidv4(), placeholder: "Alternative 4..." }
      ]
    }
  },
  methods: {
    getData() {
      for(let i=0; i<5; i++) {
        if(app.$children[i].questions!=undefined) {
          return app.$children[i].questions;
        }
      }
    },
    removeQuestion() {
      var data = this.getData();
      if(data.length>1) {
        data.splice(this.index, 1);
      }
      this.getIDs();
    },
    getIDs() {
      var data = this.getData();
      for(let i=0; i<data.length; i++) {
        data[i].elID = i;
      }
    }
  },
  mounted() {
    
  }
})

const Operator = {
  template: `
    <div>
      
      <router-view></router-view>

      <div id="operator_container">

        <select id="selectMethod" v-on:change="changeMethod()">
          <option> Quiz </option>
          <option> Team distribution </option>
        </select>

        <div id="quiz_container">

          <div id="initial_container">
            <input id="title_input" type="text" placeholder="Session title..." required></input>
            <button id="start_session" v-on:click="startSession()"> Start session </button>
          </div>

          <div id="question_container">
            <question v-for="(n, index) in questions" :key="n.id" :index=n.elID></question>
          </div>

          <div id="question_adder">
            <button id="add_question" v-on:click="addQuestion()"> Add question </button> 
          </div>

        </div>

        <div id="team_container">

        </div>

      </div>
      
    </div>
  `,
  data() {
    return {
      questions: [
        { id: uuidv4(), elID: 0 }
      ]
    }
  },
  methods: {
    changeMethod() {

      var quiz = document.getElementById("quiz_container");
      var team = document.getElementById("team_container");
      var selected = document.getElementById("selectMethod")

      if(selected.value=="Quiz") {

        quiz.style.visibility = "visible";
        quiz.style.maxHeight = "1000vh";

        team.style.visibility = "hidden";
        team.style.maxHeight = "0vh";

      } else {

        team.style.visibility = "visible";
        team.style.maxHeight = "1000vh";

        quiz.style.visibility = "hidden";
        quiz.style.maxHeight = "0vh";

      }

    },
    addQuestion() {
      this.questions.push({ id: uuidv4()});
      this.getIDs();
    },
    getIDs() {
      for(let i=0; i<this.questions.length; i++) {
        this.questions[i].elID = i;
      }
    },
    startSession() {

      var title = document.getElementById("title_input");
      var questions = document.getElementsByClassName("question_input");
      var alts = document.getElementsByClassName("alt_input");
      var quiz = document.getElementById("quiz_container");

      var operator = document.getElementById("operator_container");

      var questionArr = [];
      var altArr = [];

      if(title.value!="") {

        for(let i=0; i<questions.length; i++) {
          questionArr.push(questions[i].value);
          questions[i].value = "";

          altArr.push([]);
          for(let k=(i*4); k<((i*4)+4); k++) {
            altArr[i].push(alts[k].value);
            alts[k].value = "";
          }
        }

        dataObj = new newData(title.value, questionArr, altArr);

        dataObj = testData; //for testing

        title.value = "";

        operator.style.visibility = "hidden";
        quiz.style.visibility = "hidden";
        quiz.style.maxHeight = "0vh";

        this.$router.push("/operator/session");

      } else {
        alert("You have to fill out the form");
      }

    }
  },
  beforeRouteEnter(to, from, next) {
    var page = document.getElementById("page_content");
    page.style.alignItems = "flex-start";
    next();
  },
  beforeRouteLeave(to, from, next) {
    var page = document.getElementById("page_content");
    page.style.alignItems = "center";
    next();
  },
  mounted() {
    this.getIDs();
  }
}

const session = {
  template: `
    <div id="session_container">

      <div id="session_header">
        <h2> {{ dataObj.title }} </h2>
        <button id="next_question" v-on:click="nextQuestion()"> Next </button>
      </div>

      <div id="session_question_container"> 
        <div id="session_question_display"> {{ currentQuestion+1 }}: {{ dataObj.questions[currentQuestion] }} </div>
        <div id="session_alt_container">
          <div class="session_alts" v-for="(n, index) in 4"> {{ dataObj.alts[currentQuestion][index] }} </div>
        </div>
      </div>
    </div>
  `,
  data() {
    return {
      dataObj: dataObj,
      currentQuestion: 0
    }
  },
  methods: {
    buttonColors() {
      var alts = document.getElementsByClassName("session_alts");
      var colors = ["#2BEF55", "#BA7EF5", "#FD0E0E", "#3AA5FF"];

      for(let i=0; i<alts.length; i++) {
        alts[i].style.backgroundColor = colors[i];
      }
    },
    nextQuestion() {
      if(this.currentQuestion<this.dataObj.questions.length-1) {
        this.currentQuestion++;

        socket.emit("nextQuestion");

      } else {
        this.$router.push("/results");
        socket.emit("endSession", true);
      }
    }
  },
  beforeRouteLeave(to, from, next) {
    socket.emit("endSession", false);
    next();
  },
  mounted() {
    this.$nextTick(function() {
      this.buttonColors();
      socket.emit("sessionStart", dataObj);
    })
  }
}

const Results = {
  template: `
    <div id="result_container">
      <div id="result_select_container">
        <div v-if="results!=''" id="result_selector">
          <h2> {{ results.title }} </h2>
          <select id="selectResults" v-on:change="updateResults()">
            <option v-for="(n, index) in results.questions"> {{ n }} </option>
          </select>
        </div>
        <canvas id="chart" ref="chart"></canvas>
      </div>
    </div>
  `,
  data() {
    return {
      user: "Images/google.png",
      direction: "login()",
      results: sessionResults
    }
  },
  methods: {
    showResults() {
      var canvasElement = document.getElementById("chart");
      var ctx = canvasElement.getContext("2d");

      chart = new Chart(ctx, chartConfig);
    },
    updateResults() {
      var select = document.getElementById("selectResults");

      chartConfig.data.datasets[0].data = this.results.results[select.selectedIndex]; 
      if(router.currentRoute.fullPath == "/results") {
          chart.update();
      }
    }
  },
  mounted() {
    this.$nextTick(function() {
      this.showResults();
    })
  }

}

const Client = {
  template: `
    <div id="client_container"> 
      <div id="question_display"> Et spørsmål? </div>
      <div id="button_container">
      <div class="client_button" v-for="n in 4"></div>
      </div>
    </div>
  `,
  methods: {
    buttonColors() {
      var buttons = document.getElementsByClassName("client_button");
      var colors = ["#2BEF55", "#BA7EF5", "#FD0E0E", "#3AA5FF"];

      for(let i=0; i<buttons.length; i++) {
        buttons[i].style.backgroundColor = colors[i];
        buttons[i].onclick = function() {
          socket.emit("buttonData", i);
        }
      }
    }
  },
  mounted() {
    this.$nextTick(function() {
      this.buttonColors();
    })
  }
}

const error = {
  template: `
    <div>
      Error 404: page not found
    </div>
  `,
  mounted() {
    admin();
  }
}

const routes = [
  { path: '/', redirect: "/client" },
  { path: '/operator', component: Operator, children: [{
    path: 'session', 
    component: session,
    beforeEnter: (to, from, next) => {
      if(dataObj!="") {
        next();
      } else {
        next("/404error")
      }
    }
  }] },
  { path: '/results', component: Results },
  { path: '/client', component: Client },
  { path: "*", redirect: "/404error" },
  { path: "/404error", component: error }
]

const router = new VueRouter({
  //mode: 'history',
  routes
})

//creates the vue instance
var app = new Vue({
  router
}).$mount('#app');

/*
var menu = Backbone.Model.extend({
  initializer: function() {
      console.log("Yeet")
  },
  defaults: {
      Arnt: "yeet",
      pEtteR: "halla" 
  },
  validate: (attrs) => {
      if(!attrs.title) {
          return "Title is required!";
      }
  }
});

var menu = new menu();

$(document).ready(function(){
    $("p").click(function(){
        $(this).hide();
    });
}); */
  