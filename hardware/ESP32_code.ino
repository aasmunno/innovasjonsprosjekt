// libraries: --------------------------------------------------------------------------

#include <Arduino.h>
#include <analogWrite.h>

#include <WiFi.h>           // Importerer WiFi bibliotek
#include <WiFiMulti.h>      // Importerer et til WiFi bibliotek som er nødvendig for ESP32

#include <SocketIoClient.h> // Importerer Socket.io bibliotek som også importerer WebSockets bibliotek implisitt

WiFiMulti WiFiMulti;        // Deklarerer en instans av klassen WiFiMulti
SocketIoClient webSocket;   // Deklarerer en instans av klassen SocketIoClient


// variables: --------------------------------------------------------------------------

int loopTime =    50; // Delaytime in our loop

char colour =     "red";

#define greenBTN  14  // Green button pin
#define yellowBTN 13  // Yellow (Purple) button pin
#define redBTN    27  // Red (Pink) button pin
#define blueBTN   12  // Blue button pin

#define greenLED  19  // Green LED pin
#define yellowLED 5   // Yellow (Purple) LED pin
#define redLED    21  // Red LED pin
#define blueLED   18  // Blue LED pin

#define redRGB    21
#define greenRGB  22
#define blueRGB   23


// setup functions: --------------------------------------------------------------------------

void setupButtons(){
  pinMode(greenBTN,   INPUT);
  pinMode(yellowBTN,  INPUT);  
  pinMode(blueBTN,    INPUT);
  pinMode(redBTN,     INPUT);
}

void setupLED(){
  pinMode(greenLED,   OUTPUT);
  pinMode(yellowLED,  OUTPUT);
  pinMode(redLED,     OUTPUT);
  pinMode(blueLED,    OUTPUT);
}

void setupRGB(){
  pinMode(redRGB,     OUTPUT);  
  pinMode(greenRGB,   OUTPUT);
  pinMode(blueRGB,    OUTPUT);
}

void setupWifi() {
  Serial.setDebugOutput(true);
  
  Serial.println();
  Serial.println();
  Serial.println();
  
  for(uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] BOOT WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }

  //"iPhone", "testPassord"
  WiFiMulti.addAP("AndroidAP", "fskh7627");
    
  while(WiFiMulti.run() != WL_CONNECTED) {
    Serial.println("Not connected to wifi...");
    delay(100);
  }

  Serial.println("Connected to WiFi successfully!");
}


// void setup ------------------------------------------------------------------------------

void setup() {
  Serial.begin(115200);
  setupButtons();
  setupLED();
  setupRGB();
  
  setupWifi();
  
  webSocket.on("clientConnected", event);
  // NodeServer (IP-adress, port)
  webSocket.begin("192.168.43.190", 3000);
}


// void loop() ---------------------------------------------------------------------------

void loop() {
  webSocket.loop();
  btnCheck();
  delay(loopTime);

}


//loop functions --------------------------------------------------------------------------

//on server connection
void event(const char * payload, size_t length) {
  Serial.printf("Client ID: %s\n", payload);
}

//function to check button states
void btnCheck() {
  
  int w = digitalRead(greenBTN);
  int x = digitalRead(yellowBTN);
  int y = digitalRead(redBTN);
  int z = digitalRead(blueBTN);

  if(w) {
    while(true) {
      if(!digitalRead(greenBTN)) {
        Serial.println("Green button pushed");
        
        break;
      }
    }
  }
  
  if(x) {
    while(true) {
      if(!digitalRead(yellowBTN)) {
        Serial.println("Yellow button pushed");          
        break;
      }
    }
  }
    
  if(y) {
    while(true) {
      if(!digitalRead(redBTN)) {
        Serial.println("Red button pushed");          
        break;
      }
    }
  }

  if(z) {
    while(true) {
      if(!digitalRead(blueBTN)) {
        Serial.println("Blue button pushed");          
        break;
      }
    }
  }
}

void updateLEDs(){}

void RGB(colour){
  if (colour == "red") {
    setRGBcolour(255, 0, 0)
  } else if (colour == "green") {
    setRBGcolour(0, 255, 0)
  } else {
    setRGBcolour(0,0, 255)
  }
}

void setRGBcolour(int R, int G, int B) {
  analogWrite(redRGB, R);
  analogWrite(greenRGB, G);
  analogWrite(blueRGB, B);
}

void sendData(){}
