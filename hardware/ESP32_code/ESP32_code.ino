 
  // libraries: --------------------------------------------------------------------------
  
  #include <Arduino.h>
  #include <analogWrite.h>
  
  #include <WiFi.h>           // Importerer WiFi bibliotek
  #include <WiFiMulti.h>      // Importerer et til WiFi bibliotek som er nødvendig for ESP32
  
  #include <SocketIoClient.h> // Importerer Socket.io bibliotek som også importerer WebSockets bibliotek implisitt
  
  WiFiMulti WiFiMulti;        // Deklarerer en instans av klassen WiFiMulti
  SocketIoClient webSocket;   // Deklarerer en instans av klassen SocketIoClient
  
  // variables: --------------------------------------------------------------------------
  
  int loopTime =    50; // Delaytime in our loop

  bool session = false;
  bool hasAnswered = false;
  
  String color =    "red";
  
  #define greenBTN  12  // Green button pin 
  #define yellowBTN 33  // Yellow (Purple) button pin 
  #define redBTN    34  // Red (Pink) button pin 
  #define blueBTN   26  // Blue button pin 
  
  #define greenLED  25  // Green LED pin
  #define yellowLED 14  // Yellow (Purple) LED pin
  #define redLED    27  // Red LED pin
  #define blueLED   32  // Blue LED pin
  
  #define redRGB    21
  #define greenRGB  22
  #define blueRGB   23
  
  
  // setup functions: --------------------------------------------------------------------------
  
  void setupButtons(){
    pinMode(greenBTN, INPUT);
    pinMode(yellowBTN, INPUT);  
    pinMode(redBTN, INPUT);
    pinMode(blueBTN, INPUT);
  }
  
  void setupLED(){
    pinMode(greenLED, OUTPUT);
    pinMode(yellowLED, OUTPUT);
    pinMode(redLED, OUTPUT);
    pinMode(blueLED, OUTPUT);
  }
  
  void setupRGB(){
    pinMode(redRGB, OUTPUT);  
    pinMode(greenRGB, OUTPUT);
    pinMode(blueRGB, OUTPUT);
  }
  
  void setupWifi() {
    Serial.setDebugOutput(true);
    
    Serial.println();
    Serial.println();
    Serial.println();
    
    for(uint8_t t = 4; t > 0; t--) {
      Serial.printf("[SETUP] BOOT WAIT %d...\n", t);
      Serial.flush();
      delay(1000);
    }
  
    //"iPhone", "testPassord"
    //"AndroidAP", "fskh7627"
    WiFiMulti.addAP("Telenor4GNest", "Nestingan2019");
      
    while(WiFiMulti.run() != WL_CONNECTED) {
      Serial.println("Not connected to wifi...");
      setRGBcolor("red");
      delay(100);
    }
  
    Serial.println("Connected to WiFi successfully!");
  }
  
  
  // void setup ------------------------------------------------------------------------------
  
  void setup() {
    
    Serial.begin(115200);

    setRGBcolor("red");

    setLEDs(0,4);

    // setup 
    setupButtons();
    setupLED();
    setupRGB();
    
    setupWifi();

    setLEDs(0,0);
    
    webSocket.on("clientConnected", event);
    webSocket.on("sessionStart", sessionStart);
    webSocket.on("nextQuestion", nextQuestion);
    webSocket.on("endSession", endSession);

    // NodeServer (IP-adresse, port)
    webSocket.begin("192.168.2.159", 3030);
    
  }
  
  // void loop ---------------------------------------------------------------------------
  
  void loop() {
    
    webSocket.loop();
    btnCheck();
    
    delay(loopTime);
  
  }
  
  //functions --------------------------------------------------------------------------
  
  //on server connection
  void event(const char * payload, size_t length) {
    setRGBcolor("green");
    Serial.printf("Client ID: %s\n", payload);
  }

  void sessionStart(const char * payload, size_t length) {
    String alts(payload);
    Serial.println(alts);
    session = true;
    hasAnswered = false;
    setLEDs(0, alts.toInt());
  }

   void nextQuestion(const char * payload, size_t length) {
    String alts(payload);
    Serial.println(alts);
    hasAnswered = false;
    setLEDs(0, alts.toInt());  
  }

  void endSession(const char * payload, size_t length) {
    session = false;
    hasAnswered = false;
    setLEDs(0,0);
  }
  
  //function to check button states
  void btnCheck() {
    
    int w = digitalRead(greenBTN);
    int x = digitalRead(yellowBTN);
    int y = digitalRead(redBTN);
    int z = digitalRead(blueBTN);
  
    if(w) {
      while(true) {
        if(!digitalRead(greenBTN)) {
          if(session && !hasAnswered) {
            hasAnswered = true;
            setLEDs(1,0);
          }
          webSocket.emit("buttonOne");
          break;
        }
      }
    }
    
    if(x) {
      while(true) {
        if(!digitalRead(yellowBTN)) {
          if(session && !hasAnswered) {
            hasAnswered = true;
            setLEDs(2,0);
          }  
          webSocket.emit("buttonTwo");        
          break;
        }
      }
    }
      
    if(y) {
      while(true) {
        if(!digitalRead(redBTN)) {
          if(session && !hasAnswered) {
            hasAnswered = true;
            setLEDs(3,0); 
          } 
          webSocket.emit("buttonThree");        
          break;
        }
      }
    }
  
    if(z) {
      while(true) {
        if(!digitalRead(blueBTN)) {
          if(session && !hasAnswered) {
            hasAnswered = true;
            setLEDs(4,0);
          }   
          webSocket.emit("buttonFour");      
          break;
        }
      }
    }
  }
  
  void setLEDs(int id, int num) {

    int colors[] = {0,0,0,0};

    if(num) {
      for(int i=0;i<num;i++) {
        colors[i] = 1;
      }
    } else if(id) {
      colors[id-1] = 1;
    } 

    digitalWrite(greenLED, colors[0]);
    digitalWrite(yellowLED, colors[1]);
    digitalWrite(redLED, colors[2]);
    digitalWrite(blueLED, colors[3]);

  }
  
  void setRGBcolor(String color) {

    int R = 0;
    int G = 0;
    int B = 0;

    if(color=="red") {
      R = 255;
    } else if(color=="green") {
      G = 255;
    }
    
    analogWrite(redRGB, R);
    analogWrite(greenRGB, G);
    analogWrite(blueRGB, B);
  }
