const fs = require("fs");
const http = require("http");
const express = require("express");
const app = express();
var path = require('path');

const methods = require("./methods");
const { clear } = require("console");

const serverPort = 3030;

var htmlPath = path.join(__dirname, '../frontend');

const server = http.createServer(app);
const io = require("socket.io").listen(server);

app.use(express.static(htmlPath));

var connectedClients = [];

var session = false;
var currentQuestion = 0;

var sessionResults = {};

var sessionData = [0,0,0,0]; //for testing

//HTTPS Server
server.listen(serverPort, function() {
    console.log("listening on port: " + serverPort);
    console.log();
});

//on client connection
io.on("connection", function(socket) {

    var clientID = socket.id;
    var client = io.sockets.connected[clientID];

    connectedClients.push(new methods.clientClass(clientID));

    console.log("Client connected with id: " + clientID);

    client.emit("clientConnected", clientID);

    io.emit("updateData", sessionData);

    client.on('disconnect', function() {
        connectedClients.splice(methods.getIndex(clientID, connectedClients), 1);
    });

    client.on("buttonData", function(data) {
        updateButtonData(data);
    });

    client.on("buttonOne", function() {
        updateButtonData(0);
    })

    client.on("buttonTwo", function() {
        updateButtonData(1);
    })

    client.on("buttonThree", function() {
        updateButtonData(2);
    })

    client.on("buttonFour", function() {
        updateButtonData(3);
    })

    function updateButtonData(data) {
        if(session && !connectedClients[methods.getIndex(clientID, connectedClients)].hasAnswered) {
            sessionResults.results[currentQuestion][data]++;
            connectedClients[methods.getIndex(clientID, connectedClients)].hasAnswered = true;
        }

        //for testing
        if(!connectedClients[methods.getIndex(clientID, connectedClients)].admin) {
            sessionData[data]++;
            io.emit("updateData", sessionData);
        }
    }

    client.on("admin", function() {
        connectedClients[methods.getIndex(clientID, connectedClients)].admin = true;
    });

    client.on("reset", function() {
        sessionData = [0,0,0,0];
        io.emit("updateData", sessionData);
    });

    client.on("sessionStart", function(data) {
        session = true;

        for(let i=0; i<connectedClients.length; i++) {
            connectedClients[i].hasAnswered = false;
        }

        currentQuestion = 0;
        sessionResults = new methods.sessionClass(data.title, data.questions, data.alts);

        io.emit("sessionStart", sessionResults.alts[0].length);
    });

    client.on("nextQuestion", function() {
        currentQuestion++;

        for(let i=0; i<connectedClients.length; i++) {
            connectedClients[i].hasAnswered = false;
        }

        sessionResults.results.push([0,0,0,0]);
        io.emit("nextQuestion", sessionResults.alts[currentQuestion].length)
    });

    client.on("endSession", function(complete) {
        session = false;
        if(complete) {
            io.emit("sessionResults", sessionResults);
        }

        io.emit("endSession");
    });

});

function tick() {
    io.emit("clientUpdate", connectedClients.length-1);
}

setInterval(tick, 1000);