function getIndex(clientID, connectedClients) {
  for(let i=0; i<connectedClients.length; i++) {
    if(connectedClients[i].id==clientID) {
      return i;
    }
  }
}

class clientClass {
  constructor(id) {
      this.id = id,
      this.admin = false,
      this.hasAnswered = false
  }
}

class sessionClass {
  constructor(title, questions, alternatives) {
      this.title = title,
      this.questions = questions,
      this.alts = alternatives,
      this.results = [[0,0,0,0]]
  }
}

module.exports = { getIndex, clientClass, sessionClass };